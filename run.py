# Program runs ABCD framework
# Write yields and estimate to csv file

import csv
import numpy as np
import runABCD
import getRow
from csv import reader, writer

def main(mode, cuts):

  wdir = "/home/richards/WorkArea/DPJanalysis/v02-00/frvz_vbf_wMETtrigWeights/"
  #wdir = "/home/richards/WorkArea/DPJanalysis/v02-00/withJVT/"
  outdir = "/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/"+mode+"/"

  files = ["data_run2.root", "frvz_vbf_500757.root"] # change between 757, 758, 761, 762
  #files = ["wjets_sh221.root", "wjets_ewk.root"] # closure tests wjets
  #files = ["wjets_merged.root"] # closure tests wjets
  print("******************************************")
  print("RUNNING ABCD FRAMEWORK IN MODE " + mode.upper())
  print("******************************************\n")
  # run ABCD framework
  yields = []
  with open(outdir+"output.csv", "w") as outfile:
    writer = csv.writer(outfile)
    writer.writerow([mode, "nA", "nB", "nC", "nD", "nA estimate"])
    for idx in range(len(files)):
      infile = wdir + files[idx]
      if "vbf" in files[idx]: sample = files[idx].replace("frvz_", "").replace(".root", "")
      #if "qcd" in files[idx]: sample = files[idx].replace("_main.root", "")
      else: sample = files[idx].replace(".root", "")
      yields = runABCD.main(infile, mode, sample, cuts)
      row = getRow.main(yields, mode, sample)
      writer.writerow(row)
  # tranpose table to add to cutflow
  with open(outdir+'output.csv', "r") as f, open(outdir+'output_T.csv', 'w', newline='') as fT:
    writer = csv.writer(fT)
    writer.writerows(zip(*reader(f)))
  f.close()
  fT.close()
  print("")
  print("Table with ABCD yields written to output/" + mode + "/output.csv\n") 
  #print("Program exiting...")
  print("******************************************")
  print("            PROGRAM EXITING...            ")
  print("******************************************")

###
#isoID_cuts = [6.0, 5.5, 5.0, 4.5, 4.0, 3.5, 3.0, 2.5, 2.0, 1.5, 1]
#for isoID_cut in isoID_cuts: main('estimation', cuts=[isoID_cut, 0.95])

#DPJtagger_cuts = [0.90, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98]
#for DPJtagger_cut in DPJtagger_cuts: main('estimation', cuts=[2.0, DPJtagger_cut])
###

main('estimation', cuts=[2.0, 0.9])
#main('control-region', cuts=[2.0, 0.9])
#main('sub-region-bc_validation', cuts=[4.0, 0.9]) # BC subplane
#main('sub-region-dc_validation', cuts=[2.0, 0.85]) # DC subplane
