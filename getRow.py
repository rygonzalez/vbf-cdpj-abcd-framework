import csv

def main(yields, mode, sample):
  
   pm = u"\u00B1"
   nA = str(round(yields[0], 2)) + "±" + str(round(yields[5], 2))
   nB = str(round(yields[1], 2)) + "±" + str(round(yields[6], 2))
   nC = str(round(yields[2], 2)) + "±" + str(round(yields[7], 2))
   nD = str(round(yields[3], 2)) + "±" + str(round(yields[8], 2))
   if 'data' in sample: nA_est = str(round(yields[4], 2)) + "±" + str(round(yields[9], 2))
   else: nA_est = "/"
   row = [sample, nA, nB, nC, nD, nA_est]
   return row 
