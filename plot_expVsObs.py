import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import run # ABCD framework runfile
import mplhep as hep
plt.style.use([hep.style.ATLAS, {'axes.labelsize': 20}, {'axes.labelpad': 20}, {'axes.titlelocation': 'right'}, ]) # ATLAS style
from scipy.interpolate import make_interp_spline # smooth curves

###

# Flags
sliding_isoid = False
sliding_tagger = True

met_op = False # obsolete
jetmet_op = True
subreg_dc = False
subreg_bc = False

###

if met_op==True or jetmet_op==True: outDir = "/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/control-region/"
if subreg_bc==True: outDir = "/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/sub-region-bc_validation/"
if subreg_dc==True: outDir = "/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/sub-region-dc_validation/"

def get_nAs(nAs):
  with open(outDir+"output_T.csv", "r") as inFile:
    reader = csv.reader(inFile, delimiter=",")
    count = 0
    for row in reader:
      if count == 1: nAs[0].append(row[1]) # append observed
      if count == 5: nAs[1].append(row[1]) # append expected
      count+=1
  inFile.close()
  return nAs

def isoID():
  DPJtagger_cut = 0.9
  if subreg_dc==True: DPJtagger_cut = 0.85
  nAs = [[],[]]
  print("\n")
  print("Running over leading cDPJ isoID values...")
  cuts = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5]
  if subreg_bc==True: cuts = [2.5, 3, 3.5, 4, 4.5, 5, 5.5]
  for cut in cuts:
    print("Current leading cDPJ isoID cut at: " + str(cut))
    print("\n")
    if met_op==True or jetmet_op==True: run.main("control-region", [cut, DPJtagger_cut])
    if subreg_bc==True: run.main("sub-region-bc_validation", [cut, DPJtagger_cut])
    if subreg_dc==True: run.main("sub-region-dc_validation", [cut, DPJtagger_cut])
    nAs = get_nAs(nAs)
    #print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, DPJtagger_cut, nAs, sliding_var="LJjet1_isoID")

def DPJtagger():
  isoID_cut = 2
  if subreg_bc==True: isoID_cut = 4
  nAs = [[],[]]
  print("\n")
  print("Running over leading cDPJ tagger score values...")
  cuts = [0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98] # [0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
  if subreg_dc==True: cuts = [0.82, 0.825, 0.83, 0.835, 0.84, 0.845, 0.85, 0.86, 0.87, 0.88] # [0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85]
  for cut in cuts:
    print("Current leading cDPJ tagger cut at: " + str(cut))
    print("\n")
    if met_op==True or jetmet_op==True: run.main("control-region", [isoID_cut, cut])
    if subreg_bc==True: run.main("sub-region-bc_validation", [isoID_cut, cut])
    if subreg_dc==True: run.main("sub-region-dc_validation", [isoID_cut, cut])
    nAs = get_nAs(nAs)
    #print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, isoID_cut, nAs, sliding_var="LJjet1_DPJtagger")

#######

def centrality():
  nAs = [[],[]]
  print("\n")
  print("Running over LJmu1 centrality values...")
  cuts = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
  #cuts = [0.4]
  for cut in cuts:
    print("Current LJmu1 centrality cut at " + str(cut))
    print("\n")
    run.main("control-region_validation", [4.5, cut])
    nAs = get_nAs("LJmu1 Centrality", nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, nAs, sliding_var="LJmu1_centrality")

def dphijj():
  nAs = [[],[]]
  print("\n")
  print("Running over |dphijj| values...")
  cuts = [2.5, 2.6, 2.7, 2.8, 2.9, 3.0]
  for cut in cuts:
    print("Current |dphijj| centrality cut at " + str(cut))
    print("\n")
    run.main("control-region_validation", [2, cut])
    nAs = get_nAs("|dphijj|", nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, nAs, sliding_var="|dphijj|")

def charge():
  xcut = 1
  nAs = [[],[]]
  print("\n")
  print("Running over muDPJ |charge| values...")
  cuts = [1, 2, 3, 4]
  for ycut in cuts:
    print("Current muDPJ |charge| cut at " + str(ycut))
    print("\n")
    run.main("control-region_validation", [xcut, ycut])
    nAs = get_nAs(nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, xcut, nAs, sliding_var="muDPJ_charge")

#######

# Change styling here

def plot(cuts, static_cut, nAs, sliding_var):
  print("Now plotting...")
  print("\n")
  obs_nAs = [float(nA.split("±")[0]) for nA in nAs[0]] # no error on observed nA!
  obs_errs = [np.sqrt(obs_nA) for obs_nA in obs_nAs]
  obs_errsUp = [(obs_nAs[i] - obs_errs[i]) for i in range(len(obs_nAs))]
  obs_errsDown = [(obs_nAs[i] + obs_errs[i]) for i in range(len(obs_nAs))]
  exp_nAs = [float(exp_nA.split("±")[0]) for exp_nA in nAs[1]]
  exp_errs = [float(exp_nA.split("±")[1]) for exp_nA in nAs[1]]
  exp_errsUp = [(exp_nAs[i] - exp_errs[i]) for i in range(len(exp_nAs))]
  exp_errsDown = [(exp_nAs[i] + exp_errs[i]) for i in range(len(exp_nAs))]

  # Smoothing validation curves
  #if jetmet_op == True: granul = 50
  #elif subreg_dc == True: granul = 50
  #elif subreg_bc == True: granul = 35
  cuts_smooth = np.linspace(min(cuts), max(cuts), 50)
  obs_spl = make_interp_spline(cuts, obs_nAs)
  obsUp_spl = make_interp_spline(cuts, obs_errsUp)
  obsDown_spl = make_interp_spline(cuts, obs_errsDown)
  exp_spl = make_interp_spline(cuts, exp_nAs)
  expUp_spl = make_interp_spline(cuts, exp_errsUp)
  expDown_spl = make_interp_spline(cuts, exp_errsDown)
  #
  obs_nAs_smooth  = obs_spl(cuts_smooth)
  obs_errsUp_smooth = obsUp_spl(cuts_smooth)
  obs_errsDown_smooth = obsDown_spl(cuts_smooth)
  exp_nAs_smooth  = exp_spl(cuts_smooth)
  exp_errsUp_smooth = expUp_spl(cuts_smooth)
  exp_errsDown_smooth = expDown_spl(cuts_smooth)

  #marker_index = [cuts_smooth.index(2.5), ]

  # Plot curves
  fig, ax = plt.subplots()
  if jetmet_op == True:
    ax = plt.plot(cuts_smooth, obs_nAs_smooth, label="Observed A'", linewidth=0.5) # plot observed
    ax = plt.plot(cuts_smooth, exp_nAs_smooth, label="Expected A'", linewidth=0.5) # plot expected
  elif subreg_dc == True:
    ax = plt.plot(cuts_smooth, obs_nAs_smooth, label="Observed DC1", linewidth=0.5) # plot observed
    ax = plt.plot(cuts_smooth, exp_nAs_smooth, label="Expected DC1", linewidth=0.5) # plot expected
  elif subreg_bc == True:
    ax = plt.plot(cuts_smooth, obs_nAs_smooth, label="Observed BC1", linewidth=0.5) # plot observed
    ax = plt.plot(cuts_smooth, exp_nAs_smooth, label="Expected BC1", linewidth=0.5) # plot expected
  
  # Uncertainty bands
  plt.fill_between(cuts_smooth, exp_errsUp_smooth, exp_errsDown_smooth, color="orange", alpha=0.2)
  plt.fill_between(cuts_smooth, obs_errsUp_smooth, obs_errsDown_smooth, color="blue", alpha=0.2)

  plt.scatter(cuts, obs_nAs, marker='.', color='#1f77b4') # observed
  plt.scatter(cuts, exp_nAs, marker='.', color='#ff7f0e') # observed

  # Axis labels, legend, canvas text
  if sliding_var == 'LJjet1_isoID': plt.xlabel("Cut on cDPJ $\\Sigma_{\\Delta R=0.5} p_{T}$ [GeV]")
  elif sliding_var == 'LJjet1_DPJtagger': plt.xlabel("Cut on cDPJ QCDtagger score")
  plt.ylabel("Events")
  plt.legend(loc=1) # 4 = low right
  hep.atlas.text(' Work in progress', loc=1, fontsize=16)
  plt.text(0.05,0.85,'cDPJ VBF channel', fontsize=14, transform=plt.gca().transAxes) 
  if met_op == True: # obsolete
    plt.text(0.05,0.8,'ABCD validation - MET VR', fontsize=14, transform=plt.gca().transAxes) # obsolete
  if jetmet_op == True:
    plt.text(0.05,0.8,'Inverted $|\\Delta\\phi|_{j-MET}^{min}$ VR', fontsize=14, transform=plt.gca().transAxes)
    plt.ylim(bottom=0)
  if subreg_dc == True:
    plt.text(0.05,0.8,'DC subplane', fontsize=14, transform=plt.gca().transAxes)
    plt.ylim(bottom=0)
  if subreg_bc == True:
    plt.text(0.05,0.8,'BC subplane', fontsize=14, transform=plt.gca().transAxes)
    plt.ylim(bottom=0)
  
  # Save plot
  loc = "obsVsExp_nA_"+sliding_var+".png"
  plt.savefig(outDir+loc)
  print("------------------------------------------------------")
  print("Finished - plot saved to "+outDir+loc)
  print("------------------------------------------------------")
  plt.clf()

if sliding_isoid == True: isoID()
if sliding_tagger == True: DPJtagger()

#######

#charge()
#centrality()
#dphijj()
