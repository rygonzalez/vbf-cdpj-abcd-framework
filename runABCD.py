# Main program that runs ABCD framework
# Recieves input file, applies region specific cuts
# Calculates expected nA, signal leakage & errors
# Execute ABCD estimation of nA, deals with errors and signal leakage

from ROOT import TFile, TTree, TCanvas, TH2D, TLine, kRed
import math
import numpy as np
#from extractYields import dphijj_dpjtagger, detajj_isoID, isoID_dpjtagger
import dpjtagger_isoID
import dpjtagger_centrality

def main(inFile, mode, sample, cuts):

  # get TTree
  tfile = TFile(inFile)
  ttree = tfile.Get("miniT")

  pm = u"\u00B1"
  # call program extractYields.main() to return nevents in each region of interest
  #for i in range(len(cuts)):
  nA, nB, nC, nD, n, errA, errB, errC, errD, errN = dpjtagger_isoID.main(ttree, sample, mode, cuts) # dpjtagger_isoID or dpjtagger_centrality
  bgdYields = [nB, nC, nD]
  print("--------------------------------------------------------------------")
  if mode == "estimation": print("Extracting ABCD yields from " + sample + "...")
  print("--------------------------------------------------------------------")
  # SIGNAL: calculate signal leakage into CRs
  nA_exp, errA_exp = 0, 0
  if "vbf" in sample:
    signalLeakage = [round((ni/n), 2) for ni in bgdYields]
    print("Signal in nA: ", round(nA,2), pm, round(errA,2))
    signalFraction = []
    for ni in [nA, nB, nC, nD]:
      if ni == 0: signalFraction.append(0)
      else: signalFraction.append(round(ni/n, 3))
    print("% signal fraction into [A, B, C, D]: ", signalFraction)
    print("Signal leakage into CRs [B, C, D] = " + str(signalLeakage))
  # DATA: perform ABCD calculation
  if "data" in sample: # or "wjets", only data originally
    nA_exp = (nB/nC)*nD
    # error propagation - systematic only
    if any([n == 0 for n in bgdYields]): errA_exp = 0
    else: errA_exp = nA_exp*math.sqrt((errB/nB)**2 + (errC/nC)**2 + (errD/nD)**2)
    print("Background estimation in nA: ", round(nA_exp,2), pm , round(errA_exp,2))
    if mode != 'estimation': print("Observed nA: ", round(nA,2), pm , round(errA,2))
  abcdYields = [nA, nB, nC, nD, nA_exp, errA, errB, errC, errD,  errA_exp]

  return abcdYields
