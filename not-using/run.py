# Program runs ABCD framework
# Write yields and estimate to csv file

import csv
import numpy as np
import runABCD
import getRow 
from csv import reader, writer

def main(mode):

  wdir = "/Users/s1891044/Documents/DarkPhotons/source/v01-06/"  
  outdir = "/Users/s1891044/Documents/DarkPhotons/vbf-hdpj-abcd-framework/output/isoID_dpjtagger/calTrig/"+mode+"/"  
  cutdir = "/Users/s1891044/Documents/DarkPhotons/hep-python/Cutflow"  
 
  #files = ["data_run2.root", "qcd_main.root"] # testing correlations
  #files = ["data_run2.root"] #estimation
  #files = ["qcd_main.root"] #correlation
  #files = ["frvz_vbf_500757.root", "frvz_vbf_500758.root", "frvz_vbf_500759.root", "frvz_vbf_500760.root", "frvz_vbf_500761.root", "frvz_vbf_500762.root", "data_run2.root"]
  files = ["data_run2.root", "frvz_vbf_500758.root"]
  print("******************************************")
  print("RUNNING ABCD FRAMEWORK IN MODE " + mode.upper())
  print("******************************************")
  # run ABCD framework
  yields = []
  with open(outdir+"output.csv", "w") as outfile:
    writer = csv.writer(outfile)
    writer.writerow([mode, "nA", "nB", "nC", "nD", "nA estimate"])
    for idx in range(len(files)):
      infile = wdir + files[idx]
      if "vbf" in files[idx]: sample = files[idx].replace("frvz_", "").replace(".root", "")
      if "qcd" in files[idx]: sample = files[idx].replace("_main.root", "")
      else: sample = files[idx].replace(".root", "")
      yields = runABCD.main(infile, mode, sample)
      row = getRow.main(yields, mode, sample)
      writer.writerow(row)
  # tranpose table to add to cutflow
  with open(outdir+'output.csv', "r") as f, open(outdir+'output_T.csv', 'w', newline='') as fT:
    writer = csv.writer(fT)
    writer.writerows(zip(*reader(f)))
  f.close()
  fT.close()
  print("\n")
  print("Table with ABCD yields written to output/isoID_dpjtagger/orTrig/" + mode + "/output.csv") 
  print("\n")
  print("Program exiting...")
  print("\n")

#main('estimation')
#main('correlation')
#main('sub-region_validation')
main('invert_jetmet_validation')
