# Program to count number of events in each ABCD region
# Plots ABCD plane for signal and data
# Returns yields

from ROOT import TH2D, TLine, TCanvas, TTree, TFile, kRed, TLatex, TPaveText, gPad, gStyle
import ctypes
import math
import numpy as np

###

import os
cwd = os.getcwd()
stylepath = cwd + '/ATLAS_Style/'
###print(stylepath)

import ROOT
ROOT.gROOT.LoadMacro( stylepath + "AtlasStyle.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasLabels.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasUtils.C")
ROOT.SetAtlasStyle()

from ROOT import ATLASLabel

###

def main(ttree, infile, mode, cuts):

  # Set default vars for nA estimation
  # isoID
  xbins = 20
  xmin = 0
  xcut = cuts[0]
  xmax = 20

  # dpjtagger
  ybins = 20
  ymin = 0.8
  ycut = cuts[1]
  ymax = 1

  if "vbf" in infile: weights = "(scale1fb*intLumi*metTrig_weight)" # modified to include met trigger scale factors for signal
  else: weights = "(scale1fb*intLumi)"
  vbfFilter = "(njet30>1&&mjj>1e6&&abs(detajj)>3)"
  highMET_trigger = "(metTrig==1)"
  trigger = "(hadDPJTrig==1||metTrig==1)"
  lowMET_trigger = "(hadDPJTrig==1)"
  minMET = "(MET>100e3)"
  lowMET = "(MET>100e3&&MET<225e3)"
  highMET = "(MET>225e3)"
  dpjContent = "(nLJjets20>=1&&nLJmus20==0)"
  dpjContent_newdef = "(LJ1_type==2)"
  leptonVeto = "(neleSignal==0&&nmuSignal==0)"
  bjetVeto = "(hasBjet==0)"
  BIBremoval = "(LJjet1_BIBtagger>0.2)"
  dpjQuality = "(LJjet1_gapRatio>0.9)"
  dphijj = "(abs(dphijj)<2.5)"
  jvt = "(LJjet1_jvt<0.4)"
  jetmet = "(min_dphi_jetmet>0.4)"
  centrality = "(LJjet1_centrality>0.7)"
  timing = "(abs(LJjet1_timing)<4)"
  # validation
  invert_jetmet = "(min_dphi_jetmet<0.4)"
  invert_jvt = "(LJjet1_jvt>0.4)"
  invert_dphijj = "(abs(dphijj)>2.5)"
  invert_highMET = "(MET<225e3)"
  #invert_centrality = "(LJjet1_centrality<0.5)"
  BC_sel = "&&(LJjet1_isoID*0.001>2.0)"
  DC_sel = "&&(LJjet1_DPJtagger<0.9)"
  # for w+jets closure studies
  jvt_loose = "(LJjet1_jvt<0.9)"

  # main ABCD plane
  fullhadronic_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+highMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jetmet+"&&"+jvt+"&&"+dphijj+"&&"+timing # high MET SR
  #fullhadronic_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+lowMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jetmet+"&&"+jvt+"&&"+dphijj+"&&"+timing # low MET SR
  #fullhadronic_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+minMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jetmet+"&&"+jvt+"&&"+dphijj+"&&"+timing # lower MET cut for CRs
  
  # orthogonal planes
  #invert_jetmet_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+highMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+invert_jetmet+"&&"+jvt+"&&"+timing # no dphijj
  invert_jetmet_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+minMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+invert_jetmet+"&&"+jvt+"&&"+dphijj+"&&"+timing # lower MET cut
  #invert_jvt_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+highMET+"&&"+dpjContent+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+invert_jvt # no jetmet, dphijj
  #invert_dphijj_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+highMET+"&&"+dpjContent+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jvt+"&&"+invert_dphijj # no jetmet
  #invert_highMET_presel = weights+"*("+vbfFilter+"&&"+highMET_trigger+"&&"+invert_highMET+"&&"+dpjContent_newdef+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jetmet+"&&"+jvt+"&&"+dphijj+"&&"+timing
  # subplanes
  BC_presel = fullhadronic_presel + BC_sel
  DC_presel = fullhadronic_presel + DC_sel

  if "estimation" in mode: presel = fullhadronic_presel
  if "control-region" in mode: presel = invert_jetmet_presel # invert_jetmet, highMET
  if "sub-region-bc" in mode: presel, xmin, xbins = BC_presel, 2.0, 18
  if "sub-region-dc" in mode: presel, ymax, ybins = DC_presel, 0.9, 20

  # blinding cut
  #if "data" in infile and mode=="estimation": presel += "&&(LJjet1_DPJtagger<"+str(ycut)+"||LJjet1_isoID*0.001>="+str(xcut)+")" # comment for unblinding

  # get nevents in each region
  hA = TH2D("hA", "hA", xbins, xmin, xcut, ybins, ycut, ymax)
  hA.Sumw2()
  hB = TH2D("hB", "hB", xbins, xcut, xmax, ybins, ycut, ymax)
  hB.Sumw2()
  hC = TH2D("hC", "hC", xbins, xcut, xmax, ybins, ymin, ycut)
  hC.Sumw2()
  hD = TH2D("hD", "hD", xbins, xmin, xcut, ybins, ymin, ycut)
  hD.Sumw2()
  h = TH2D("h_"+infile, "LJjet1_isoID*0.001:LJjet1_DPJtagger ", xbins, xmin, xmax, ybins, ymin, ymax)
  h.Sumw2()
  print("ymin, ycut, ymax: ", ymin, ycut, ymax)
  print("xmin, xcut, xmax: ", xmin, xcut, xmax)

  # define regional cuts
  cutA = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<"+str(xcut)+"&&LJjet1_DPJtagger>="+str(ycut)+"&&LJjet1_DPJtagger<="+str(ymax)+")"
  cutB = "(LJjet1_isoID*0.001>="+str(xcut)+"&&LJjet1_isoID*0.001<"+str(xmax)+"&&LJjet1_DPJtagger>="+str(ycut)+"&&LJjet1_DPJtagger<="+str(ymax)+")"
  cutC = "(LJjet1_isoID*0.001>="+str(xcut)+"&&LJjet1_isoID*0.001<"+str(xmax)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<"+str(ycut)+")"
  cutD = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<"+str(xcut)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<"+str(ycut)+")"
  cutABCD = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<="+str(xmax)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<="+str(ymax)+")"

  # errors for TH1 integral
  errA = ctypes.c_double(0)
  errB = ctypes.c_double(0)
  errC = ctypes.c_double(0)
  errD = ctypes.c_double(0)
  errN = ctypes.c_double(0)

  # create histogram and extract nevents for each region ABCD
  canvas = TCanvas("c", "c", 10, 10, 700, 700) # was 700,700 w/o ATLAS style
  canvas.SetRightMargin(0.2) # 0.12 w/o ATLAS style
  canvas.SetLeftMargin(0.15) # 0.12 w/o ATLAS style
  canvas.SetTopMargin(0.1) # use with ATLAS style
  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hA", (presel+"*"+cutA+")"), "colz")
  nA = hA.IntegralAndError(0, xbins, 0, ybins, errA)

  canvas.Clear()
  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hB", (presel+"*"+cutB+")"), "colz")
  nB = hB.IntegralAndError(0, xbins, 0, ybins, errB) 
  canvas.Clear()

  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hC", (presel+"*"+cutC+")"), "colz")
  nC = hC.IntegralAndError(0, xbins, 0, ybins, errC)
  canvas.Clear()

  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hD", (presel+"*"+cutD+")"), "colz")
  nD = hD.IntegralAndError(0, xbins, 0, ybins, errD)
  canvas.Clear()

  canvas.cd()

  if "data" in infile: title = "\\mbox{Full Run 2 Data: cDPJ isoID vs. cDPJ tagger score}"
  if "vbf" in infile: title = "\\mbox{"+str(infile.replace("frvz_", "").replace("_", " "))+": cDPJ isoID vs. cDPJ tagger score}"

  # draw histogram of total ABCD plane and save to file
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>h_"+infile, (presel+"*"+cutABCD+")"), "colz")
  nh = h.IntegralAndError(0, xbins, 0, ybins, errN)
  n = h.IntegralAndError(0, xbins+1, 0, ybins+1, errN)
  gPad.Update()
  #h.SetNameTitle(infile, title) # commented to make code work for wjets
  #h.SetXTitle("cDPJ isoID [GeV]")
  h.SetXTitle("#Sigma_{#Delta R=0.5} p_{T} [GeV]")
  h.SetYTitle("cDPJ tagger score")
  h.SetZTitle("Events")
  h.GetZaxis().CenterTitle(True)
  h.GetZaxis().SetTitleOffset(1.5)
  #if "data" in infile: h.SetMinimum(1)
  h.SetMinimum(0)
  h.SetMaximum(100)
  h.GetXaxis().SetRange(0, xbins)
  h.GetYaxis().SetRange(0, ybins)
  h.GetYaxis().SetRangeUser(ymin, ymax)
  h.GetXaxis().SetRangeUser(xmin, xmax)
  h.GetXaxis().SetLabelSize(0.045) # use with ATLAS style
  h.GetYaxis().SetLabelSize(0.045) # use with ATLAS style
  h.GetXaxis().SetLabelOffset(0.015)
  print("Without overflow: ", nh)
  print("With overflow: ", n)
  #stats = h.FindObject("stats")
  #stats.SetX1NDC(0.65)
  #stats.SetX2NDC(0.85)
  #stats.SetY1NDC(0.8)
  #stats.SetY2NDC(0.6)
  #gStyle.SetOptStat(0)
  corr = round(h.GetCorrelationFactor(1, 2), 4)*100
  if mode == "correlation" or mode == "control-region" or mode == "sub-region-bc_validation" or mode == "sub-region-dc_validation" or "wjets" in infile: print("ABCD plane correlation: " + str(corr) + "%")
  # BC sub-region validation
  if mode == "sub-region-bc_validation":
    BC1 = TPaveText(xcut-2,ycut+0.3,xcut-0.5,ycut-0.2) # xcut-2,ycut-0.5,xcut-0.5,ycut-0.2
    BC2 = TPaveText(xcut+12,ycut+0.3,xcut-0.5,ycut-0.2) # xcut-2,ycut+0.2,xcut-0.5,ycut+0.5
    BC3 = TPaveText(xcut+12,ycut+0.1,xcut-0.5,ycut-0.2) # xcut+0.5,ycut+0.2,xcut+2,ycut+0.5
    BC4 = TPaveText(xcut-2,ycut+0.1,xcut-0.5,ycut-0.2) # xcut+0.5,ycut-0.5,xcut+2,ycut-0.2
    BC1.SetBorderSize(0)
    BC2.SetBorderSize(0)
    BC3.SetBorderSize(0)
    BC4.SetBorderSize(0)
    BC1.SetTextSize(0.04) # 0.045
    BC2.SetTextSize(0.04)
    BC3.SetTextSize(0.04)
    BC4.SetTextSize(0.04)
    BC1.SetFillStyle(0)
    BC2.SetFillStyle(0)
    BC3.SetFillStyle(0)
    BC4.SetFillStyle(0)
    BC1.SetTextAlign(12)
    BC2.SetTextAlign(12)
    BC3.SetTextAlign(12)
    BC4.SetTextAlign(12)
    BC1.AddText("BC1");
    BC2.AddText("BC2");
    BC3.AddText("BC3");
    BC4.AddText("BC4");
    BC1.Draw()
    BC2.Draw()
    BC3.Draw()
    BC4.Draw()
  elif mode == "sub-region-dc_validation":
    DC1 = TPaveText(xcut-2,ycut+0.25,xcut-0.5,ycut-0.2) # xcut-2,ycut+0.3,xcut-0.5,ycut-0.2
    DC2 = TPaveText(xcut+12,ycut+0.25,xcut-0.5,ycut-0.2) # xcut+12,ycut+0.3,xcut-0.5,ycut-0.2
    DC3 = TPaveText(xcut+12,ycut+0.15,xcut-0.5,ycut-0.2) # xcut+12,ycut+0.1,xcut-0.5,ycut-0.2
    DC4 = TPaveText(xcut-2,ycut+0.15,xcut-0.5,ycut-0.2) # xcut-2,ycut+0.1,xcut-0.5,ycut-0.2
    DC1.SetBorderSize(0)
    DC2.SetBorderSize(0)
    DC3.SetBorderSize(0)
    DC4.SetBorderSize(0)
    DC1.SetTextSize(0.035) # 0.4
    DC2.SetTextSize(0.035)
    DC3.SetTextSize(0.035)
    DC4.SetTextSize(0.035)
    DC1.SetFillStyle(0)
    DC2.SetFillStyle(0)
    DC3.SetFillStyle(0)
    DC4.SetFillStyle(0)
    DC1.SetTextAlign(12)
    DC2.SetTextAlign(12)
    DC3.SetTextAlign(12)
    DC4.SetTextAlign(12)
    DC1.AddText("DC1");
    DC2.AddText("DC2");
    DC3.AddText("DC3");
    DC4.AddText("DC4");
    DC1.Draw()
    DC2.Draw()
    DC3.Draw()
    DC4.Draw()
  elif "control" in mode or mode == "estimation":
    A = TPaveText(xcut-1.30,ycut+0.01,xcut-0.25,ycut+0.04) # xcut-0.75 first argument
    B = TPaveText(xcut+0.25,ycut+0.01,xcut+0.75,ycut+0.04)
    C = TPaveText(xcut+0.25,ycut-0.04,xcut+0.75,ycut-0.01)
    D = TPaveText(xcut-1.30,ycut-0.04,xcut-0.25,ycut-0.01) # xcut-0.75 first argument
    A.SetBorderSize(0)
    B.SetBorderSize(0)
    C.SetBorderSize(0)
    D.SetBorderSize(0)
    A.SetTextSize(0.05)
    B.SetTextSize(0.05)
    C.SetTextSize(0.05)
    D.SetTextSize(0.05)
    A.SetFillStyle(0)
    B.SetFillStyle(0)
    C.SetFillStyle(0)
    D.SetFillStyle(0)
    A.SetTextAlign(12)
    B.SetTextAlign(12)
    C.SetTextAlign(12)
    D.SetTextAlign(12)
    if "control" in mode:
      A.AddText("A'");
      B.AddText("B'");
      C.AddText("C'");
      D.AddText("D'");
    else:
      A.AddText("A");
      B.AddText("B");
      C.AddText("C");
      D.AddText("D");
    A.Draw();
    B.Draw();
    C.Draw();
    D.Draw();
  if mode == "correlation":
    E = TPaveText(12,1.4,17,1.8) #12,1.4,17,1.8
    E.SetBorderSize(1)
    E.SetTextSize(0.05)
    E.SetTextColor(2)
    E.SetTextAlign(12)
    E.AddText(str(round(corr*100, 2)) + "% correlation");
    E.Draw();
  xcut = TLine(xcut, ymin, xcut, ymax)
  xcut.SetLineColor(kRed)
  xcut.SetLineWidth(2)
  ycut = TLine(xmin, ycut, xmax, ycut)
  ycut.SetLineColor(kRed)
  ycut.SetLineWidth(2)
  if "correlation" not in mode:
    xcut.Draw("same")
    ycut.Draw("same")

  #canvas.SetLogz()
  h.SetStats(False) # remove stats box from histogram
  #ATLASLabel(0.15,0.92,"Internal") # can be used when include ATLAS_Style
  if "data" in infile:
    ATLASLabel(0.15,0.92,"") # can be used when include ATLAS_Style
    texdata = ROOT.TLatex(0.565,0.92,"#sqrt{s}=13 TeV, 139 fb^{-1}")
    texdata.SetNDC(True)
    texdata.SetTextSize(0.0325)
    texdata.Draw()
    #texsignalregion = ROOT.TLatex(0.545,0.845,"caloDPJ low E_{T}^{miss}")
    texsignalregion = ROOT.TLatex(0.535,0.845,"caloDPJ high E_{T}^{miss}")
    texsignalregion.SetNDC(True)
    texsignalregion.SetTextSize(0.0325)
    texsignalregion.Draw()
  if "vbf" in infile:
    ATLASLabel(0.15,0.92,"Simulation") # can be used when include ATLAS_Style
    texdata = ROOT.TLatex(0.565,0.92,"#sqrt{s}=13 TeV, 139 fb^{-1}") # 0.54,0.84 when using Simulation Internal in label
    texdata.SetNDC(True)
    texdata.SetTextSize(0.0325)
    texdata.Draw()
  if "500757" in infile:
    #texsignalregion = ROOT.TLatex(0.545,0.845,"caloDPJ low E_{T}^{miss}")
    texsignalregion = ROOT.TLatex(0.515,0.845,"caloDPJ high E_{T}^{miss}")
    texsignalregion.SetNDC(True)
    texsignalregion.SetTextSize(0.0325)
    texsignalregion.Draw()
    texsignal = ROOT.TLatex(0.44,0.80,"m_{#gamma_{d}}=0.1 GeV, c#tau_{#gamma_{d}}=15 mm") # 0.38,0.73; 0.38,0.25
    texsignal.SetNDC(True)
    texsignal.SetTextSize(0.0325)
    texsignal.Draw()
  if mode != "estimation":
    texcorr = ROOT.TLatex(0.6,0.2,"#rho = %.2f" %h.GetCorrelationFactor()) # 0.67, 0.2
    texcorr.SetNDC(True)
    texcorr.SetTextSize(0.045)
    texcorr.Draw()
  
  #texdata.SetTextAlign(32)
  #texsignalregion.SetTextAlign(32)

  canvas.Print("/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/" + mode + "/" + infile + "_160424.png")
  canvas.Print("/home/richards/WorkArea/DPJanalysis/repositories/vbf-cdpj-abcd-framework/output/" + mode + "/" + infile + "_160424.pdf")
  print("Plot saved to output/" + mode + "/" + infile + ".png")
  # return nevents in each region + stat. error
  vals = [nA, nB, nC, nD, n, errA.value, errB.value, errC.value, errD.value, errN.value]
  if "data" not in infile:
    print("nA: ", str(round(nA, 1)), "±", round(errA.value, 1))
    print("nB: ", str(round(nB, 1)), "±", round(errB.value, 1))
    print("nC: ", str(round(nC, 1)), "±", round(errC.value, 1))
    print("nD: ", str(round(nD, 1)), "±", round(errD.value, 1))
  else:
    print("nA: ", str(round(nA, 1)))
    print("nB: ", str(round(nB, 1)))
    print("nC: ", str(round(nC, 1)))
    print("nD: ", str(round(nD, 1)))
  if "data" not in infile: print("n:  ", round(n, 1), "±", round(errN.value, 1))
  print("")
  canvas.Close()
  return vals
