# Program to count number of events in each ABCD region
# Plots ABCD plane for signal and data
# Returns yields

from ROOT import TH2D, TLine, TCanvas, TTree, TFile, kRed, TLatex, TPaveText, gPad, gStyle
import ctypes
import math
import numpy as np


def main(ttree, infile, mode):

  # Set default vars for nA estimation
  # LJjet1 DPJ tagger 
  xbins = 80
  xmin = 0.2
  xmax = 1 
  xcut = 0.95

  # LJjet1 isoID
  ybins = 40
  ymax = 20
  ycut = 2
  ymin = 0

  vbfPass = "jet1_pt>90e3&&abs(jet1_eta)<3.2&&jet2_pt>80e3&&abs(jet2_eta)<4.9&&abs(detajj)>4.0&&abs(dphijj)<2.0&&mjj>1250e3&&RunNumber>=355529"
  weights = "(scale1fb*intLumi)"
  vbfFilter = "(njet30>1&&mjj>1e6&&abs(detajj)>3)"
  trigger = "(hadDPJTrig==1)"#||(trig[55]==1&&"+vbfPass+"))"
  metRegion = "(MET>80e3&&MET<225e3)"
  dpjContent = "(nLJjets20>0&&nLJmus20==0)"
  leptonVeto = "(neleSignal==0&&nmuSignal==0)"
  bjetVeto = "(hasBjet==0)"
  BIBremoval = "(LJjet1_BIBtagger>0.2)"
  dpjTag = "(LJjet1_DPJtagger>=0.2)"
  fakeRemoval = "(LJjet1_BIBtagger>0.2&&LJjet1_DPJtagger>=0.2)"
  dpjQuality = "(LJjet1_gapRatio>0.9&&LJjet1_jvt<0.4)"
  jvt = "(LJjet1_jvt<0.4)"
  invert_jvt = "(LJjet1_jvt>0.4)"
  crackVeto = "(LJjet1_gapRatio>0.9)"
  jetmet = "(min_dphi_jetmet>0.4)"
  invert_jetmet = "(min_dphi_jetmet<0.4)"
  abcd = "(LJjet1_isoID*0.001<=20&&LJjet1_isoID>=0&&LJjet1_DPJtagger>=0.2&&LJjet1_DPJtagger<=1)"
  jz2_veto = "(dsid!=364702)"
  dphijj = "abs(dphijj)<2.5"

  highStat_presel = weights+"*("+trigger+"&&"+vbfFilter+"&&"+jz2_veto+"&&"+metRegion+"&&"+dpjContent+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+fakeRemoval+"&&"+crackVeto
  fullhadronic_presel = weights+"*("+vbfFilter+"&&"+jz2_veto+"&&"+trigger+"&&"+metRegion+"&&"+dpjContent+"&&"+abcd+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+fakeRemoval+"&&"+dpjQuality+"&&"+jetmet+"&&"+dphijj
  jvtValidation_presel = weights+"*("+vbfFilter+"&&"+jz2_veto+"&&"+trigger+"&&"+metRegion+"&&"+dpjContent+"&&"+abcd+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+fakeRemoval+"&&"+crackVeto+"&&"+invert_jvt
  jetmet_validation_presel = weights+"*("+vbfFilter+"&&"+trigger+"&&"+metRegion+"&&"+dpjContent+"&&"+abcd+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+fakeRemoval+"&&"+dpjQuality+"&&"+invert_jetmet+"&&"+jz2_veto
  invertBoth_validation_presel = weights+"*("+vbfFilter+"&&"+jz2_veto+"&&"+trigger+"&&"+metRegion+"&&"+dpjContent+"&&"+abcd+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+fakeRemoval+"&&"+crackVeto+"&&"+invert_jvt+"&&"+invert_jetmet

  if mode == "estimation" or mode == "sub-region_validation": presel = fullhadronic_presel
  elif mode == "invert_jetmet_validation":  presel = jetmet_validation_presel

  if "data" in infile and presel == fullhadronic_presel and mode == "estimation": presel += "&&(LJjet1_DPJtagger<"+str(xcut)+"||LJjet1_isoID*0.001>="+str(ycut)+")" 
  
  # get nevents in each region
  hA = TH2D("hA", "hA", xbins, xcut, xmax, ybins, ymin, ycut)
  hA.Sumw2()
  hB = TH2D("hB", "hB", xbins, xcut, xmax, ybins, ycut, ymax)
  hB.Sumw2()
  hC = TH2D("hC", "hC", xbins, xmin, xcut, ybins, ycut, ymax)
  hC.Sumw2()
  hD = TH2D("hD", "hD", xbins, xmin, xcut, ybins, ymin, ycut)
  hD.Sumw2()
  h = TH2D("h_"+infile, "LJjet1_DPJtagger:LJjet1_isoID*0.001", xbins, xmin, xmax, ybins, ymin, ymax)
  h.Sumw2()

  print("xmin, xcut, xmax: ", xmin, xcut, xmax)
  print("ymin, ycut, ymax: ", ymin, ycut, ymax)
  # define regional cuts
  cutA = "(LJjet1_DPJtagger>="+str(xcut)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&LJjet1_isoID*0.001>="+str(ymin)+"&&LJjet1_isoID*0.001<"+str(ycut)+")"
  cutB = "(LJjet1_DPJtagger>="+str(xcut)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&LJjet1_isoID*0.001>="+str(ycut)+"&&LJjet1_isoID*0.001<="+str(ymax)+")"
  cutC = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<"+str(xcut)+"&&LJjet1_isoID*0.001>="+str(ycut)+"&&LJjet1_isoID*0.001<="+str(ymax)+")"
  cutD = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<"+str(xcut)+"&&LJjet1_isoID*0.001>="+str(ymin)+"&&LJjet1_isoID*0.001<"+str(ycut)+")"
  cutABCD = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&LJjet1_isoID*0.001>="+str(ymin)+"&&LJjet1_isoID*0.001<="+str(ymax)+")"

  # errors for TH1 integral
  errA = ctypes.c_double(0)
  errB = ctypes.c_double(0)
  errC = ctypes.c_double(0)
  errD = ctypes.c_double(0)
  errN = ctypes.c_double(0)
  
  # create histogram and extract nevents for each region ABCD
  canvas = TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  ttree.Draw("LJjet1_isoID*0.001:LJjet1_DPJtagger>>hA", (presel+"*"+cutA+")"), "colz")
  nA = hA.IntegralAndError(0, xbins+1, 0, ybins+1, errA)
  print("nA", nA)

  canvas.Clear()
  canvas.cd()
  ttree.Draw("LJjet1_isoID*0.001:LJjet1_DPJtagger>>hB", (presel+"*"+cutB+")"), "colz")
  nB = hB.IntegralAndError(0, xbins+1, 0, ybins+1, errB) 
  canvas.Clear()
  print("nB", nB)
  
  canvas.cd()
  ttree.Draw("LJjet1_isoID*0.001:LJjet1_DPJtagger>>hC", (presel+"*"+cutC)+")", "colz")
  nC = hC.IntegralAndError(0, xbins+1, 0, ybins+1, errC)
  canvas.Clear()
  print("nC", nC)

  canvas.cd()
  ttree.Draw("LJjet1_isoID*0.001:LJjet1_DPJtagger>>hD", (presel+"*"+cutD+")"), "colz")
  nD = hD.IntegralAndError(0, xbins+1, 0, ybins+1, errD)
  canvas.Clear()
  print("nD", nD)

  canvas.cd()
  
  if "qcd" in infile: title = "\\mbox{QCD: cDPJ Tagger Score vs. cDPJ isoID}"
  if "data" in infile: title = "\\mbox{Full Run 2 Data: cDPJ Tagger Score vs. cDPJ isoID}"
  if "vbf" in infile: title = "\\mbox{"+str(infile.replace("frvz_", "").replace("_", " "))+": cDPJ Tagger Score vs. cDPJ isoID}"
  if "500758" in infile: title = "\\mbox{VBF Benchmark: cDPJ Tagger Score vs. cDPJ isoID}"

  # draw histogram of total ABCD plane and save to file
  ttree.Draw("LJjet1_isoID*0.001:LJjet1_DPJtagger>>h_"+infile, (presel+"*"+cutABCD+")"), "colz")
  nh = h.IntegralAndError(0, xbins, 0, ybins, errN)
  n = h.IntegralAndError(0, xbins+1, 0, ybins+1, errN)
  gPad.Update()
  h.SetNameTitle(infile, title)
  h.SetXTitle("\mathrm{cDPJ\;Tagger\;Score}")
  h.SetYTitle("\mathrm{cDPJ\;isoID\;(GeV)}")
  h.GetXaxis().SetRange(0, xbins)
  h.GetYaxis().SetRange(0, ybins)
  print("without overflow", nh)
  print("with overflow", n)
  stats = h.FindObject("stats")
  stats.SetX1NDC(0.15)
  stats.SetX2NDC(0.35)
  stats.SetY1NDC(0.85)
  stats.SetY2NDC(0.65)
  #gStyle.SetOptStat(0)
  corr = round(h.GetCorrelationFactor(1, 2), 4)
  if mode == "correlation": print("ABCD plane correlation: " + str(corr)) 
  # CD sub-region validation
  if xmax == 0.95 and mode == "sub-region_validation":
    CD1 = TPaveText(xcut+0.01,ycut-1.0,xcut+0.045,ycut-0.2)
    CD2 = TPaveText(xcut+0.01,ycut+0.2,xcut+0.045,ycut+1.0)
    CD3 = TPaveText(xcut-0.045,ycut+0.2,xcut-0.01,ycut+1.0)
    CD4 = TPaveText(xcut-0.045,ycut-1.0,xcut-0.01,ycut-0.2)
    CD1.SetBorderSize(1)
    CD2.SetBorderSize(1)
    CD3.SetBorderSize(1)
    CD4.SetBorderSize(1)
    CD1.SetTextSize(0.03)
    CD2.SetTextSize(0.03)
    CD3.SetTextSize(0.03)
    CD4.SetTextSize(0.03)
    CD1.SetFillColor(0)
    CD2.SetFillColor(0)
    CD3.SetFillColor(0)
    CD4.SetFillColor(0)
    CD1.SetTextAlign(12)
    CD2.SetTextAlign(12)
    CD3.SetTextAlign(12)
    CD4.SetTextAlign(12)
    CD1.AddText("CD1");
    CD2.AddText("CD2");
    CD3.AddText("CD3");
    CD4.AddText("CD4");
    CD1.Draw()
    CD2.Draw()
    CD3.Draw()
    CD4.Draw()
  elif "invert" in mode or mode == "estimation":
    A = TPaveText(xcut+0.05,ycut-1.9,xcut+0.01,ycut-0.3)
    B = TPaveText(xcut+0.05,ycut+0.3,xcut+0.01,ycut+1.9)
    C = TPaveText(xcut-0.01,ycut+0.3,xcut-0.05,ycut+1.9)
    D = TPaveText(xcut-0.01,ycut-1.9,xcut-0.05,ycut-0.3)
    '''A = TPaveText(xcut+0.01,ycut-0.8,xcut+0.05,ycut-0.1)
    B = TPaveText(xcut+0.01,ycut+0.1,xcut+0.05,ycut+0.8)
    C = TPaveText(xcut-0.05,ycut+0.1,xcut-0.01,ycut+0.8)
    D = TPaveText(xcut-0.05,ycut-0.8,xcut-0.01,ycut-0.1)'''
    A.SetBorderSize(0)
    B.SetBorderSize(0)
    C.SetBorderSize(0)
    D.SetBorderSize(0)
    A.SetTextSize(0.07)
    B.SetTextSize(0.07)
    C.SetTextSize(0.07)
    D.SetTextSize(0.07)
    '''A.SetFillColor(0)
    B.SetFillColor(0)
    C.SetFillColor(0)
    D.SetFillColor(0)'''
    A.SetFillStyle(0)
    B.SetFillStyle(0)
    C.SetFillStyle(0)
    D.SetFillStyle(0)
    A.SetTextAlign(12)
    B.SetTextAlign(12)
    C.SetTextAlign(12)
    D.SetTextAlign(12)
    A.AddText("A'");
    B.AddText("B'");
    C.AddText("C'");
    D.AddText("D'");
    A.Draw();
    B.Draw();
    C.Draw();
    D.Draw();
  if mode == "correlation": 
    E = TPaveText(0.5,16,0.75,18)
    E.SetBorderSize(1)
    E.SetTextSize(0.05)
    E.SetTextColor(2)
    E.SetTextAlign(12)
    E.AddText(str(round(corr*100, 2)) + "% correlation");
    E.Draw();
  xcut = TLine(xcut, ymin, xcut, ymax)
  xcut.SetLineColor(kRed)
  xcut.SetLineWidth(2)
  ycut = TLine(xmin, ycut, xmax, ycut)
  ycut.SetLineColor(kRed)
  ycut.SetLineWidth(2)
  if "correlation" not in mode:
    xcut.Draw("same")
    ycut.Draw("same")
  canvas.SetLogz()
  canvas.Print("/home/richards/WorkArea/DPJanalysis/other_repos/vbf-hdpj-abcd-framework/output/" + mode + "/" + infile + ".png")
  print("Plot saved to output/" + mode + "/" + infile + ".png")
  # return nevents in each region + stat. error
  vals = [nA, nB, nC, nD, n, errA.value, errB.value, errC.value, errD.value, errN.value]
  print(nA, nB, nC, nD)
  if "data" not in infile: print("n: ", n, "±", errN.value)  
  return vals

